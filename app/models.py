from django.db import models


class Recipe(models.Model):
    title = models.CharField(max_length=31, blank=True, null=True)
    summary = models.CharField(max_length=200, blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"#{self.id} {self.title or '(untitled)'}"


class Ingredient(models.Model):
    name_singular = models.CharField(max_length=31, blank=True, null=True)
    name_plural = models.CharField(max_length=31, blank=True, null=True)

    def __str__(self):
        return f"#{self.id} {self.name_singular or '(untitled)'}"


class Unit(models.Model):
    name = models.CharField(max_length=31, blank=True, null=True)

    def __str__(self):
        return f"#{self.id} {self.name or '(untitled)'}"


class Tag(models.Model):
    name = models.CharField(max_length=31, blank=True, null=True)

    def __str__(self):
        return f"#{self.id} {self.name or '(untitled)'}"


class RecipeTag(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

    def __str__(self):
        return f"#{self.recipe} / {self.tag}"


class RecipeIngredientUnit(models.Model):
    # recipe, ingredient, ingredient
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE, default=None)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)

    # value, unit_is_displayed
    value = models.FloatField(default=0.0)
    unit_is_displayed = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.recipe} / {self.ingredient} / {self.unit}"



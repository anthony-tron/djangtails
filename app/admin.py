from django.contrib import admin

from app.models import Recipe, RecipeIngredientUnit, Tag, RecipeTag
from app.models import Ingredient
from app.models import Unit


class RecipeIngredientUnitInlineAdmin(admin.TabularInline):
    model = RecipeIngredientUnit


class RecipeAdmin(admin.ModelAdmin):
    inlines = (RecipeIngredientUnitInlineAdmin, )


admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Ingredient)
admin.site.register(Unit)
admin.site.register(Tag)
admin.site.register(RecipeTag)
admin.site.register(RecipeIngredientUnit)

from django.contrib import messages
from django.views.generic import View, TemplateView, ListView, DetailView, CreateView, UpdateView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.http import JsonResponse

from app.forms.login import LoginForm
from app.models import Recipe, Ingredient
from django.contrib.auth import authenticate, login

# Create your views here.

class IndexView(TemplateView):
    http_method_names = ['get']
    template_name = 'index.html'

    def get_context_data(self, **kwargs):

        # Note: cool syntax, but it's about two times slower
        return {
            **super().get_context_data(**kwargs),
            'title': 'Yet another new Djangtails',
        }



class RecipeListView(ListView):
    template_name = 'recipe_list.html'
    model = Recipe


# first: DetailView
# then: DetailView
class RecipeDetailView(LoginRequiredMixin, DetailView):
    template_name = 'recipe_detail.html'
    model = Recipe

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['ingredients'] = Ingredient.objects.filter(
            recipeingredientunit__recipe__pk=self.object.pk
        )
        return result


class IngredientCreateView(LoginRequiredMixin, CreateView):
    template_name = 'ingredient_create.html'
    model = Ingredient
    fields = ('name_singular', 'name_plural')
    success_url = reverse_lazy('ingredient_list')


class IngredientUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'ingredient_create.html'
    model = Ingredient
    fields = ('name_singular', 'name_plural')
    success_url = reverse_lazy('ingredient_list')


class IngredientListView(ListView):
    template_name = 'ingredient_list.html'
    model = Ingredient


class LoginFormView(FormView):
    template_name = 'login.html'
    form_class = LoginForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(self.request, user)
            messages.add_message(
                self.request, messages.INFO,
                f'Hello  {user.username}!'
            )
            return super().form_valid(form)
        form.add_error(None, 'Username et mot de passe invalides.')
        return self.form_invalid(form)


class IngredientsApiAll(View):
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        ingredients = Ingredient.objects.all()
        return JsonResponse(
            [
                {
                    "id": i.pk,
                    "name": i.name_singular,
                } for i in ingredients  # <- something terrible happens if you add a comma here
            ],
            safe=False,
        )
